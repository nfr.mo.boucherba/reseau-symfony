<?php

// This file has been auto-generated by the Symfony Dependency Injection Component for internal use.

if (\class_exists(\ContainerO1TGHHg\App_KernelDevDebugContainer::class, false)) {
    // no-op
} elseif (!include __DIR__.'/ContainerO1TGHHg/App_KernelDevDebugContainer.php') {
    touch(__DIR__.'/ContainerO1TGHHg.legacy');

    return;
}

if (!\class_exists(App_KernelDevDebugContainer::class, false)) {
    \class_alias(\ContainerO1TGHHg\App_KernelDevDebugContainer::class, App_KernelDevDebugContainer::class, false);
}

return new \ContainerO1TGHHg\App_KernelDevDebugContainer([
    'container.build_hash' => 'O1TGHHg',
    'container.build_id' => '2526db33',
    'container.build_time' => 1649015057,
], __DIR__.\DIRECTORY_SEPARATOR.'ContainerO1TGHHg');
