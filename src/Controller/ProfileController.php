<?php

namespace App\Controller;

use App\Entity\Users;
use App\Form\UsersType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

class ProfileController extends AbstractController
{
    #[Route('/profile', name: 'app_profile')]
    public function portfolio(ManagerRegistry $doctrine, Request $request): Response
    {
        $form = $doctrine->getRepository(Users::class)->findAll();
        $form = $this->createForm(UsersType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
              
       }



        return $this->render('profile/index.html.twig', [
            'controller_name' => 'Bienvenue dans votre Profile',
            'form' => $form->createView(),
        ]);
    }
}
