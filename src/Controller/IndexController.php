<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Users;
use App\Entity\Posts;
use App\Form\PostType;
use DateTimeImmutable;


class IndexController extends AbstractController
{
    #[Route('/', name: 'app_index')]
    public function index(ManagerRegistry $doctrine, REQUEST $request, EntityManagerInterface $entityManager): Response
    {
        $posts = $doctrine->getRepository(Posts::class)->findAll();

        $post = new Posts();
        $form = $this->createForm(PostType::class);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid())
        {           
            $user = $this->getUser();
            $post = $form->getData();
            $dt = new DateTimeImmutable();
            $post->setDate($dt);
            $post->setUserId($user);
            $post->setIsVisble(1);
            $entityManager->persist($post);
            $entityManager->flush();
        }
        return $this->render('index/index.html.twig', [
            'title' =>'IndexController',
            'form' => $form->createView(),
            'data' => $posts,
        ]);
    }
}
