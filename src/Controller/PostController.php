<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Comments;
use App\Form\CommentType;
use App\Entity\Posts;
use DateTimeImmutable;

class PostController extends AbstractController
{

    #[Route('/post/{id}', name: 'postdetails')]
    public function postdetails($id, ManagerRegistry $doctrine, Request $request,EntityManagerInterface $entityManager): Response 
    {
        $postdetail = $doctrine->getRepository(Posts::class)->find($id);

        $comment = new Comments();
        $form = $this->createForm(CommentType::class);
        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $user = $this->getUser();
            $comment = $form->getData();
            $dt = new DateTimeImmutable();
            $comment->SetDate($dt);
            $comment->SetPost($postdetail);
            $comment->setUser($user);
            $entityManager->persist($comment);
            $entityManager->flush();
        }

        return $this->render('post/details.html.twig', [
            'title' => 'Post Details'.$id,
            'details' => $postdetail,
            'form' => $form->createView(),
        ]);
    }

}
